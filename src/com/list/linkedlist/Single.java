package com.list.linkedlist;


/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 *
 *  points
 *  1)list data structure 
 *  2)it stores object reference of next node to locate
 *  3)insertion and deletion happens at o(1) time
 *  4)search takes 
 *
 *
 *
 *
 *
 */
public class Single {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
       
        
        Employee ganeshan = new Employee(1,"ganeshan","nagarajan");
        Employee yale = new Employee(2,"yale","morgan");
        Employee joma = new Employee(3,"jonathan","ma");
        
        EmployeeLinkedList list = new EmployeeLinkedList();
        list.addFront(ganeshan);
        list.addFront(yale);
        list.addFront(joma);
        
        
        list.printLinkedList();
    }

}
