package com.list.linkedlist;

/**
 * @author Dhiva Hema
 *
 */
public class LLImplementation {

	/**
	 * @param args
	 */
	private static Node head;
	private Node tail;
	private int size = 0;

	private class Node {
		int node_value;
		Node next;

		public Node(int node_value) {
			super();
			this.node_value = node_value;
		}
	}

	public void addFront(int node_value) {
		Node node = new Node(node_value);
		if (isEmpty())
			head = tail = node;
		else {
			node.next = head;
			head = node;
			size++;
		}

	}

	public void addLast(int node_value) {
		Node node = new Node(node_value);
		if (isEmpty())
			head = tail = node;
		else {
			tail.next = node;
			tail = node;
			node.next = null;
			size++;
		}
	}

	public boolean isContains(int node_value) {
		Node cur = head;
		while (cur != null) {
			if (node_value == cur.node_value)
				return true;
			cur = cur.next;
		}
		return false;
	}

	public void printList(Node head) {
		Node cur = head;
		while (cur != null) {
			System.out.print(" " + cur.node_value + " ");
			cur = cur.next;
		}
		System.out.println();
	}

	public void printList() {
		Node cur = head;
		while (cur != null) {
			System.out.print(" " + cur.node_value + " ");
			cur = cur.next;
		}
		System.out.println();
	}

	private int listSize() {
		return size;
	}

	private boolean isEmpty() {
		return head == null;
	}

	private void reverseList() {
		Node cur = head;
		/*
		 * temp = a a = b b = temp
		 * 
		 * next = cur.next cur.next = pre pre = cur
		 * 
		 */
		Node pre = null;
		Node next = null;
		while (cur != null) {
			next = cur.next;
			cur.next = pre;
			pre = cur;
			cur = next;
		}
		head = pre;
	}

	private Node reverseSubList(Node head, int m, int n) {

		if (head == null || m == n) {
			return head;
		}

		Node cur = head;
		Node pre = null;
		for (int i = 0; cur != null && i < m - 1; ++i) {
			pre = cur;
			cur = cur.next;
		}
		Node LastNodeOfFirstPart = pre;
		Node LastNodeOfSubList = cur;
		Node next = null;

		for (int i = 0; cur != null && i < n - m + 1; i++) {
			next = cur.next;
			cur.next = pre;
			pre = cur;
			cur = next;
		}

		if (LastNodeOfFirstPart != null) {
			LastNodeOfFirstPart.next = pre;
		} else {
			head = pre;
		}
		LastNodeOfSubList.next = cur;
		return head;
	}

	private Node nativeRemoveNthNodeFromEnd(Node head, int n) {

		if (head == null) {
			return null;
		}

		int size = 0;
		Node cur = head;
		while (cur != null) {
			size++;
			cur = cur.next;
		}
		n = size - n - 1;
		if (n <= 0) {
			head.next = head.next.next;
			return head;
		}

		cur = head;
		for (int i = 0; i < size; i++) {
			if (i == n) {
				cur.next = cur.next.next;
				break;
			}
			cur = cur.next;
		}

		return head;
	}

	private Node removeNthNodeFromEnd(Node head, int n) {
		Node dummy_node = new Node(0);
		dummy_node.next = head;
		Node first = dummy_node;
		Node second = dummy_node;
		int counter = 0;
		while (counter < n) {
			first = first.next;
			counter += 1;
		}

		while (first.next != null) {
			first = first.next;
			second = second.next;
		}
		second.next = second.next.next;
		return dummy_node.next;
	}

	private boolean detectloop(Node head) {
		Node fastptr = head;
		Node slowptr = head;
		while (slowptr != null && fastptr != null && fastptr.next != null) {
			fastptr = fastptr.next.next;
			slowptr = slowptr.next;
			if (fastptr == slowptr) {
				return true;
			}
		}
		return false;
	}

	private Node detectloopNode(Node head) {
		Node fastptr = head;
		Node slowptr = head;
		boolean loop_exists = false;
		while (slowptr != null && fastptr != null && fastptr.next != null) {
			fastptr = fastptr.next.next;
			slowptr = slowptr.next;
			if (fastptr == slowptr) {
				loop_exists = true;
				break;
			}
		}
		if (loop_exists) {
			for (slowptr = head; slowptr != fastptr; slowptr = slowptr.next, fastptr = fastptr.next)
				;
			return slowptr;
		}
		return null;
	}

	private int nodeCount(Node cur) {
		int counter = 0;
		while (cur != null) {
			cur = cur.next;
			counter += 1;
		}
		return counter;
	}

	private Node intersectPoint(Node head1, Node head2) {
		int l1 = nodeCount(head1);
		int l2 = nodeCount(head2);
		int diff = Math.abs(l1 - l2);
		Node intersect_node;
		if (diff == 0 && head1==head2) {
			return head1;
		}
		if (l1 > l2) {
			intersect_node = _intersectPoint(diff, head1, head2);
		} else {
			intersect_node = _intersectPoint(diff, head2, head1);
		}
		return intersect_node;
	}

	private Node _intersectPoint(int diff, Node h1, Node h2) {
		Node cur1 = h1;
		Node cur2 = h2;
		for (int i = 0; i < diff; i++) {
			cur1 = cur1.next;
		}
		while (cur1 != null) {
			if (cur1 == cur2) {
				break;
			}
			cur1 = cur1.next;
			cur2 = cur2.next;
		}
		return cur1;
	}

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LLImplementation list = new LLImplementation();
		list.addLast(10);
		list.addLast(20);
		list.addLast(30);
		list.addLast(40);
		list.addLast(50);
		list.addLast(60);
		list.addLast(70);
//		list.addFront(2);
//		list.addFront(1);

		LLImplementation list1 = new LLImplementation();
		list1.addLast(100);
		list1.addLast(200);

//		System.out.println(list.isContains(9876));
//		System.out.println(list.listSize());
		list.printList(head);
//		list.reverseList();
//		list.printList();
//		list.reverseSubList(head, 2, 5);
		list.printList(list.removeNthNodeFromEnd(head, 1));
	}
}