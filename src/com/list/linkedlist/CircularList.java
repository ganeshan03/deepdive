package com.list.linkedlist;

public class CircularList {

	Node head;
	Node tail;
	int size = 0;

	public class Node {
		int node_value;
		private Node next;

		public Node(int node_value) {
			this.node_value = node_value;
		}

		public Node() {
		}
	}

	private void addFront(int node_value) {
		Node node = new Node(node_value);
		if (isEmpty()) {
			head = tail = node;
		} else {
			node.next = head;
			head = node;
			size++;
		}
	}

	private void addLast(int node_value) {
		Node node = new Node(node_value);
		if (isEmpty()) {
			head = tail = node;
		} else {
			tail.next = node;
			tail = node;
			node.next = head;
			size++;
		}
	}

	private void printlist() {
		Node cur;
		cur = head;
		do {
			System.out.print(" " + cur.node_value + " ");
			cur = cur.next;
		} while (cur != head);
		System.out.println();
	}

	private boolean isEmpty() {
		return head == null;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularList clist = new CircularList();
		clist.addFront(10);
		clist.addFront(20);
		clist.printlist();

	}

}
