package com.list;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 *
 */

/**
 * points
 *  1)it allow duplicates
 *  2)list data structure
 *  3)insertion o(n)
 *  4)insertion with index o(1)
 *  5)not sync (not thread safe)
 *  6) 50% grow
 *  7)ordered
 *  8) it backed by array only so it has same ad & disad except dynamically grow
 *  9) Use vector in place of ArrayList if you need synchronization
 *  
 */

    


public class Arraylist {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        List<Employee> emplist = new ArrayList<>();
        emplist.add(new Employee(1,"ganeshan","nagarajan"));
        emplist.add(new Employee(2,"yale","morgan"));
        emplist.add(new Employee(3,"jonathan","ma"));
//        inside lambda function is working
//        emplist.forEach(employee -> System.out.println(employee));
        
//        System.out.println(emplist.get(0));
        
//        .set update the record
        emplist.set(1, new Employee(2,"patrick","techlead"));
//        added item 
        emplist.add(new Employee(2,"new patrick","millinore"));
        emplist.forEach(employee -> System.out.println(employee));
        
//        gives size of list
        System.out.println(emplist.size());
//        convert 
        Employee employeearray [] = emplist.toArray(new Employee[emplist.size()]);
        
        for(Employee i:employeearray){
            System.out.println(i);
        }
        System.out.println(emplist.contains(new Employee(1,"ganeshan","nagarajan")));
        System.out.println(emplist.indexOf(new Employee(1,"ganeshan","nagarajan")));
//        remove from array list
        emplist.remove(2);
        
        emplist.forEach(employee -> System.out.println(employee));
        
    }

}
