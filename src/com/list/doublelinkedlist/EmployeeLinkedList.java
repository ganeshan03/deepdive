package com.list.doublelinkedlist;



/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 *
 */
public class EmployeeLinkedList {
    private EmployeeNode head;
    private EmployeeNode tail;
    private int size;
    
    public void addFront(Employee employee) {
        EmployeeNode node = new EmployeeNode(employee);
        node.setNext(head);
        head = node;
        size++;
    }
    
    
    public void printLinkedList(){
        EmployeeNode current = head;
        System.out.print("head ->");
        while(current != null){
            System.out.print(current);
            System.out.print("--->");
            current = current.getNext();
            System.out.println();
        }
        System.out.print("null");
        
    }
    
    public boolean isEmpty(){
        return head == null;
    }
    
    public EmployeeNode removeFront(){
        if(isEmpty()){
           return null; 
        }
        EmployeeNode removeNode = head;
        head = head.getNext();
        size--;
        removeNode.setNext(null);
        return removeNode;
        
    }

}
