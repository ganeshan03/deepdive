package com.sorting;

/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 *
 */
public class Bubble {

    /**
     * @param args
     */
    
    int bubbleSort(int arr[]){
        int n = arr.length;
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i-1; j++) {
                if(arr[j] > arr[j+1]){
                 temp = arr[j];
                 arr[j]=arr[j+1];
                 arr[j+1]=temp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        return 0;
        
    }
    
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub
       int arr[]  = {10,34,53,534,654,3,54,345,24534,654};
       Bubble ob   = new Bubble();
       ob.bubbleSort(arr);

    }

}
