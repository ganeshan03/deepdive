package com.search.algorithm;

public class LinearSearch {
	
	public boolean linearSearch(int [] num,int value) {
		for(int i : num) {
			if(i==value) {
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num [] = {10,26,54,3,4,3,42,34,434,34,3};
		LinearSearch ls = new LinearSearch();
		if(ls.linearSearch(num,34454)) {
			System.out.println("found");
		}
		else
		{
			System.out.println("not found");
		}

	}

}
