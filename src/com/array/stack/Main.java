package com.array.stack;

/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 * 
 * https://www.cs.usfca.edu/~galles/visualization/StackArray.html
 *
 */

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        ArrayStack st = new ArrayStack(10);
        st.push(new Employee(1,"ganeshan","nagarajan"));
        st.push(new Employee(2,"hello","world"));
        
        System.out.println("Popped: " + st.pop());
        System.out.println(st.peek());
    }
}
