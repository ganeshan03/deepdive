package com.array.stack;
import java.util.EmptyStackException;

/**
 * TODO: Document me!
 *
 * @author ganeshan.nagarajan
 *
 */
public class ArrayStack {
    
    private int top;
    private Employee stack[];
    
    public ArrayStack(int capacity){
        stack = new Employee[capacity];
    }
    
    
    
    public void push(Employee employee){
        if (stack.length == top) {
            Employee newarray[] = new Employee[2*stack.length];
            // Use of arraycopy() method 
            //System.arraycopy(source_arr, sourcePos, dest_arr,  destPos, len);
            System.arraycopy(stack, 0, newarray, 0, stack.length);
            stack = newarray;
        }
        stack[top++]=employee;
        
    }
    
    
    
    public Employee pop(){
        if (isEmpty())
            throw new EmptyStackException();
        Employee employee = stack[--top];
        stack[top] = null;
        return employee;
    }
    
    
    
    public Employee peek(){
        if (isEmpty())
            throw new EmptyStackException();
        return stack[top-1];
    }
    
    
    
    
    public int size(){
        return top;
    }
    
    
    
    
    public boolean isEmpty(){
        return top == 0;
    }
}
