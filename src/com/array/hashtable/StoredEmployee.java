package com.array.hashtable;

public class StoredEmployee {

	String key;
	Employee employee;

	public StoredEmployee(String key, Employee employee) {
		this.key = key;
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "StoredEmployee [key=" + key + ", employee=" + employee + "]";
	}
}
