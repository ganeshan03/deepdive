package com.array.hashtable;

public class ArrayHashtable {

	private StoredEmployee[] hashtable;

	public ArrayHashtable() {
		hashtable = new StoredEmployee[10];
	}

//hash function
	private int hashKey(String key) {
		return key.length() % hashtable.length;
	}

	private boolean isoccupied(int hashkey) {
		return hashtable[hashkey] != null;
	}

	public void put(String key, Employee employee) {
		int hashkey = hashKey(key);
		int stopindex = hashkey;
		if (isoccupied(hashkey)) {
//			linear probing implemented
			hashkey++; // initial probe for next position
			while (isoccupied(hashkey) && hashkey != stopindex) {
				if (hashkey == hashtable.length - 1) {
					hashkey = 0;
				} else {
					hashkey = (hashkey + 1) % hashtable.length;
				}
			}
		}

		if (isoccupied(hashkey))
			System.out.println("hash table is full");
		else
			hashtable[hashkey] = new StoredEmployee(key, employee);
	}

//	find key for get function
	public int findKey(String key) {
		int hashkey = hashKey(key);
		int stopindex = hashkey;
		if (isoccupied(hashkey) && hashtable[hashkey].key.equals(key)) {
			return hashkey;
		} else {
			hashkey++;
			while (isoccupied(hashkey) && hashkey != stopindex && !hashtable[hashkey].key.equals(key)) {
				if (hashkey == hashtable.length - 1) {
					hashkey = 0;
				} else {
					hashkey = (hashkey + 1) % hashtable.length;
				}
			}
		}
		if (isoccupied(hashkey) && hashtable[hashkey].key.equals(key)) {
			return hashkey;
		} else {
			return -1;
		}
	}

	public StoredEmployee get(String key) {
		int hashkey = findKey(key);
		if (hashkey == -1)
			System.out.println("Record not found");

		return hashtable[hashkey];
	}

	public void remove(String key) {
		int hashkey = findKey(key);
		if (hashkey == -1)
			System.out.println("Record not found");
		else {
			hashtable[hashkey] = null;
//			creating new hashtable bcoz of handling null
			StoredEmployee[] oldhashtable = hashtable;
			hashtable = new StoredEmployee[oldhashtable.length];
			for (int i = 0; i < oldhashtable.length; i++) {
				if (oldhashtable[i] != null) {
					put(oldhashtable[i].key, oldhashtable[i].employee);
				}
			}
		}
	}

	public void printhashtable() {
		for (int i = 0; i < hashtable.length; i++) {
			if (hashtable[i] == null) {
				System.out.println("empty");
			} else {
				System.out.println("Position: " + i + " " + hashtable[i].employee.getFirstname());
			}
		}
		System.out.println();
	}
}
