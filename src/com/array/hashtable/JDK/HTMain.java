package com.array.hashtable.JDK;

import java.util.HashMap;
import java.util.Map;

/*
 * Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Output: Because nums[0] + nums[1] == 9, we return [0, 1].
 * 
 * 
 * 
 * 
 * hashmap - not sync - not thread safety
 * hashtable - sync - thread safety
 * 
 * 
 * */

public class HTMain {

	public void twoSum(int[] num, int target) {
		int val = 0;
		Map<Integer, Integer> ht = new HashMap<Integer, Integer>();
		for (int i = 0; i < num.length; i++) {
			val = target - num[i];
			if (ht.get(val) != null) {
				System.out.println("pair " + ht.get(val) + " " + i);
				break;
			}
			ht.put(num[i], i);
		}
	}

	public void containsDuplicate(int num[]) {
		Map<Integer, Integer> ht = new HashMap<Integer, Integer>();
		for (int i = 0; i < num.length; i++) {
			if (ht.get(num[i]) != null) {
				System.out.println("Contains duplicate");
				break;
			}
			ht.put(num[i], i);
		}

	}

	/*
	 * Example 1:
	 * 
	 * Input: nums = [1,2,3,1], k = 3 Output: true Example 2:
	 * 
	 * Input: nums = [1,0,1,1], k = 1 Output: true Example 3:
	 * 
	 * Input: nums = [1,2,3,1,2,3], k = 2 Output: false
	 */
	public void containsDuplicateII(int num[], int k) {
		Map<Integer, Integer> ht = new HashMap<Integer, Integer>();
		for (int i = 0; i < num.length; i++) {
			if (ht.get(num[i]) != null) {
				if (k >= (i - ht.get(num[i]))) {
					System.out.println("true");
					break;
				}
			}
			ht.put(num[i], i);
		}
		System.out.println("false");
	}

	/*
	 * Given an array of integers, find out whether there are two distinct indices i
	 * and j in the array such that the absolute difference between nums[i] and
	 * nums[j] is at most t and the absolute difference between i and j is at most
	 * k.
	 * 
	 * 
	 * 
	 * Example 1:
	 * 
	 * Input: nums = [1,2,3,1], k = 3, t = 0 Output: true Example 2:
	 * 
	 * Input: nums = [1,0,1,1], k = 1, t = 2 Output: true Example 3:
	 * 
	 * Input: nums = [1,5,9,1,5,9], k = 2, t = 3 Output: false
	 */

	public void containsDuplicateIII(int num[], int k, int t) {
		Map<Integer, Integer> ht = new HashMap<Integer, Integer>();
		for (int i = 0; i < num.length - k; i++) {
			for (int j = i + 1; j <= i + k; j++) {
				if (t >= Math.abs(num[i] - num[j])) {
					System.out.println("true");
					break;
				}
			}
		}
		System.out.println("false");
	}

	public void countDuplicate(int[] num) {
		Map<Integer, Integer> ht = new HashMap<Integer, Integer>();
		int value = 0;
		for (int i : num) {
			if (ht.get(i) != null) {
				 value = ht.get(i);
				ht.put(i, value + 1);
				value = 0;
			} else {
				ht.put(i, 1);
			}
		}

		ht.forEach((k, v) -> System.out.println("Key " + k + " value " + v));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HTMain htp = new HTMain();
		int target = 4;
		int[] num = { 1, 5, 9, 1, 5, 9,534,34,5,4,3,4,53,45,345,3445,345,345,345,34 };
//		htp.twoSum(num, target);
//		htp.containsDuplicate(num);
//		htp.containsDuplicateII(num, 2);
//		htp.containsDuplicateIII(num, 2, 3);
		htp.countDuplicate(num);

	}

}
