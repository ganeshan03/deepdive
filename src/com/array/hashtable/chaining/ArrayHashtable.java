package com.array.hashtable.chaining;

public class ArrayHashtable {
	public class Node {
		public Node(StoredEmployee employee) {
			this.employee = employee;
		}

		StoredEmployee employee;
		Node next;
	}

	private Node[] hashtable;

	public ArrayHashtable() {
		hashtable = new Node[10];
	}

//hash function
	private int hashKey(String key) {
		return key.length() % hashtable.length;
	}

	private boolean isoccupied(int hashkey) {
		return hashtable[hashkey] != null;
	}

	public void put(String key, Employee employee) {
		int hashkey = hashKey(key);
		if (isoccupied(hashkey)) {
			Node head = hashtable[hashkey];
			Node entry = new Node(new StoredEmployee(key, employee));
			entry.next = head;
			hashtable[hashkey] = entry;
		} else {
			Node head = new Node(new StoredEmployee(key, employee));
			hashtable[hashkey] = head;
		}
	}

	public void printhashtable() {
		for (int i = 0; i < hashtable.length; i++) {
			if (hashtable[i] == null) {
				System.out.println("null");
			} else {
				Node head = hashtable[i];
				while (head != null) {
					System.out.println("Position: " + i + " " + head.employee);
					head = head.next;
				}
			}
		}
		System.out.println();
	}
}
