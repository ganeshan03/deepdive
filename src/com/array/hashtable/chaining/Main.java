package com.array.hashtable.chaining;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee ganeshan = new Employee(1, "ganeshan", "nagarajan");
		Employee akshay = new Employee(2, "akshay", "rao");
		Employee yash = new Employee(3, "yash", "rao");
		Employee mani = new Employee(4, "mani", "rao");
		Employee ram = new Employee(5, "ram", "rao");
		Employee Adhi = new Employee(6, "Adhi", "rao");
		Employee umesh = new Employee(7, "umesh", "rao");
		Employee siva = new Employee(8, "siva", "rao");
		Employee mano = new Employee(9, "mano", "rao");

		ArrayHashtable ht = new ArrayHashtable();
		ht.put("ganeshan", ganeshan);
		ht.put("akshay", akshay);
		ht.put("yash", yash);
		ht.put("mani", mani);
		ht.put("ram", ram);
		ht.put("Adhi", Adhi);
		ht.put("umesh", umesh);
		ht.put("siva", siva);
		ht.put("mano", mano);
		ht.put("ganeshan", ganeshan);

//		ht.printhashtable();
////		ht.remove("ganeshan");
////		ht.remove("akshay");
//		ht.printhashtable();
//		System.out.println("get field value  ===> " + ht.get("yash"));
////		ht.remove("ganeshan");
////		ht.remove("ganeshan");
////		ht.remove("ram");
////		ht.remove("Adhi");
////		ht.remove("umesh");
//		ht.put("umesh", umesh);


		ht.printhashtable();
	}

}
