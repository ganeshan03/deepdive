package com.language.recursion;

public class LearnRecursion {
	
	public void fun(int n) {
		if (n>0) {
			System.out.print(n);
			fun(n-1);
			System.out.print(n);
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LearnRecursion lr = new LearnRecursion();
		lr.fun(3);
	}

}
