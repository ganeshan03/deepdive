package com.tree.binarysearchtree;

public class BinarySearchTree {

	static TreeNode root;

	public class TreeNode {
		int value;
		TreeNode left;
		TreeNode right;

		public TreeNode(int value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "TreeNode [value=" + value + "]";
		}

	}

	public void insert(int value) {
		TreeNode node = new TreeNode(value);
		if (root == null) {
			root = node;
			return;
		}

		TreeNode current = root;
		while (true) {

			if (current.value == value) {
				System.out.println("Node already exists");
				return;
			}

			if (current.value > value) {
				if (current.left == null) {
					current.left = node;
					return;
				}
				current = current.left;
			} else {
				if (current.right == null) {
					current.right = node;
					return;
				}
				current = current.right;
			}
		}

	}

	public void delete(int value) {
		if (root == null || root.value == value)
			return;
		TreeNode current = root;
		while (current != null) {
			if (current.value > value) {
				if (current.left.value == value) {
					if (current.left.left == null) {
						current.left = null;
						return;
					}
					current.left = current.left.left;

					return;
				}
				current = current.left;
			} else {
				if (current.right.value == value) {
					if (current.right.right == null) {
						current.right = null;
						return;
					}
					current.right = current.right.right;
					return;
				}
				current = current.right;
			}
		}
	}

	int findMinValue(TreeNode root) {
		while (root.left != null) {
			findMinValue(root.left);
		}
		return root.value;
	}

	public TreeNode deleterec(TreeNode root, int value) {
//		base case
		if (root == null)
			return null;
//go to exact delete node
		if (root.value > value)
			root.left = deleterec(root.left, value);
		else if (root.value < value)
			root.right = deleterec(root.right, value);
		else {
//			one child

			if (root.left == null && root.right == null) {
				root = null;

			} else {
				if (root.left == null)
					return root.right;
				else if (root.right == null)
					return root.left;
//				two child

				root.value = findMinValue(root.right);
//				repl.value = (Integer) null;
				root.right = deleterec(root.right, root.value);
			}

		}

		return root;
	}

//public static TreeNode deleteNodeIteratively(TreeNode root, int value) {
//        TreeNode parent = null, current = root;
//        boolean hasLeft = false;
//
//        if (root == null)
//            return root;
//
//        while (current != null) {
//            if ((int) current.data == value) {
//                break;
//            }
//
//            parent = current;
//            if (value < (int) current.data) {
//                hasLeft = true;
//                current = current.left;
//            } else {
//                hasLeft = false;
//                current = current.right;
//            }
//        }
//
//
//        if (parent == null) {
//            return deleteNodeIteratively(current);
//        }
//
//        if (hasLeft) {
//            parent.left = deleteNodeIteratively(current);
//        } else {
//            parent.right = deleteNodeIteratively(current);
//        }
//
//        return root;
//    }
//
//    private static TreeNode deleteNodeIteratively(TreeNode node) {
//
//        if (node != null) {
//            if (node.left == null && node.right == null) {
//                return null;
//            }
//
//            if (node.left != null && node.right != null) {
//                TreeNode inOrderSuccessor = deleteInOrderSuccessorDuplicate(node);
//                node.value = inOrderSuccessor.value;
//            } else if (node.left != null) {
//                node = node.left;
//            } else {
//                node = node.right;
//            }
//        }
//
//        return node;
//    }

//    private static TreeNode deleteInOrderSuccessorDuplicate(TreeNode node) {
//        TreeNode parent = node;
//        node = node.right;
//        boolean rightChild = node.left == null;
//
//        while (node.left != null) {
//            parent = node;
//            node = node.left;
//        }
//
//        if (rightChild) {
//            parent.right = node.right;
//        } else {
//            parent.left = node.right;
//        }
//
//        node.right = null;
//        return node;
//    }

//	DFS - pre,in,post traversal implemented

	public void preOrderTraversal() {
		preOrderTraversal(root);
		System.out.println();

	}

	public void inOrderTraversal() {
		inOrderTraversal(root);
		System.out.println();

	}

	public void postOrderTraversal() {
		postOrderTraversal(root);
		System.out.println();

	}

	private void preOrderTraversal(TreeNode root) {
		if (root == null) {
			return;
		}
		System.out.print(root.value + " ");
		preOrderTraversal(root.left);
		preOrderTraversal(root.right);
	}

	private void inOrderTraversal(TreeNode root) {
		if (root == null) {
			return;
		}
		inOrderTraversal(root.left);
		System.out.print(root.value + " ");
		inOrderTraversal(root.right);
	}

	private void postOrderTraversal(TreeNode root) {
		if (root == null) {
			return;
		}
		postOrderTraversal(root.left);
		postOrderTraversal(root.right);
		System.out.print(root.value + " ");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinarySearchTree bst = new BinarySearchTree();
//		[5,3,6,2,4,null,7]
		bst.insert(5);
		bst.insert(3);
		bst.insert(6);
		bst.insert(2);
		bst.insert(4);
		bst.insert(7);
//		bst.insert(4);
//		bst.delete(10);
//		bst.delete(9);
		bst.deleterec(root, 5);
		bst.preOrderTraversal();
		bst.inOrderTraversal();
		bst.postOrderTraversal();
	}

}
