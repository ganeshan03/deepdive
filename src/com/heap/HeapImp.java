package com.heap;

/**
 * @author Ganeshan N
 *
 * 
 *         left = parent*2+1 right = parent*2+2 parent = (index-1)/2
 *
 *
 */
public class HeapImp {

	private int[] heaparr = new int[10];
	private int size;

	public void insert(int value) {
		if (isfull())
			throw new IllegalStateException("heap is full");

		heaparr[size++] = value;
		int index = size - 1;
		bubbleUp(index);
	}

//	value rotation
	private void bubbleUp(int index) {
		int parent_index = parent(index);
		while (index > 0 && heaparr[index] > heaparr[parent_index]) {
			swap(index, parent_index);
			index = parent(index);
		}
	}

	public void remove() {
		if (isEmpty())
			throw new IllegalStateException("Heap is empty");

		heaparr[0] = heaparr[--size];
		heaparr[size] = 0;
		int index = 0;
		while (!isValidParent(index)) {
			int largerindex = heaparr[rightChildIndex(index)] < heaparr[leftChildIndex(index)] ? leftChildIndex(index)
					: rightChildIndex(index);
			swap(index, largerindex);
			index = largerindex;
		}

	}

	public int removesort() {
		if (isEmpty())
			throw new IllegalStateException("Heap is empty");
		int value = heaparr[0];
		heaparr[0] = heaparr[--size];
		heaparr[size] = 0;
		int index = 0;
		while (!isValidParent(index)) {
			int largerindex = heaparr[rightChildIndex(index)] < heaparr[leftChildIndex(index)] ? leftChildIndex(index)
					: rightChildIndex(index);
			swap(index, largerindex);
			index = largerindex;
		}
		return value;
	}

	public void heapSort() {
		int sortedarr[] = new int[heaparr.length];
		for (int i = 0; i < heaparr.length; i++) {
			sortedarr[i] = removesort();
		}
		printsort(sortedarr);
	}

	private void swap(int first, int second) {
		int temp = heaparr[first];
		heaparr[first] = heaparr[second];
		heaparr[second] = temp;
	}

	private boolean isValidParent(int index) {

		if (!hasLeftChild(index))
			return true;
		if (!hasRightChild(index))
			return heaparr[index] > heaparr[leftChildIndex(index)];

		return heaparr[index] > heaparr[leftChildIndex(index)] && heaparr[index] > heaparr[rightChildIndex(index)];
	}

	private int parent(int index) {
		return (index - 1) / 2;
	}

	private int leftChildIndex(int index) {
		return (index * 2) + 1;
	}

	private int rightChildIndex(int index) {
		return (index * 2) + 2;
	}

	private boolean hasLeftChild(int index) {
		return leftChildIndex(index) <= size;
	}

	private boolean hasRightChild(int index) {
		return rightChildIndex(index) <= size;
	}

	private boolean isfull() {
		return heaparr.length == size;
	}

	private boolean isEmpty() {
		return size == 0;
	}

	public void print() {
		for (int i : heaparr) {
			System.out.println(i);
		}
	}

	public void printsort(int sortedarr[]) {
		for (int i : sortedarr) {
			System.out.println(i);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HeapImp hi = new HeapImp();
		hi.insert(10);
		hi.insert(20);
		hi.insert(15);
		hi.insert(3);
		hi.print();
		hi.heapSort();
//		hi.remove();
//		hi.remove();
//		hi.remove();
		hi.print();
	}
}